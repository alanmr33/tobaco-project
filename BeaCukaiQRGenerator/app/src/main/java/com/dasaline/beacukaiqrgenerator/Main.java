package com.dasaline.beacukaiqrgenerator;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dasaline.beacukaiqrgenerator.Model.ItemQR;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import cz.msebera.android.httpclient.Header;

public class Main extends AppActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public AsyncHttpClient client;
    public ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Button btnSave = (Button) findViewById(R.id.buttonSave);
        final EditText textName = (EditText) findViewById(R.id.editTextName);
        final EditText textDesc1 = (EditText) findViewById(R.id.editTextDesc1);
        final EditText textDesc2 = (EditText) findViewById(R.id.editTextDesc2);
        client = new AsyncHttpClient();
        loading = new ProgressDialog(this);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = textName.getText().toString();
                String desc1 = textDesc1.getText().toString();
                String desc2 = textDesc2.getText().toString();
                loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                loading.setMessage("Connecting to Server");
                loading.setCanceledOnTouchOutside(false);
                loading.setIndeterminate(true);
                loading.show();
                RequestParams params = new RequestParams();
                params.put("name",name);
                params.put("desc_1",desc1);
                params.put("desc_2",desc2);
                client.post(Constants.APP_SAVE_URL,params, new JsonHttpResponseHandler(){
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        try {
                            statusResponse(response);
                            loading.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            default:
                break;
            case R.id.nav_logout :
                session.setLogin(false);
                Intent logout = new Intent(getApplicationContext(),Login.class);
                startActivity(logout);
                finish();
                break;
            case R.id.nav_settings :
                Intent settings = new Intent(getApplicationContext(),Settings.class);
                startActivity(settings);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    protected void statusResponse(JSONObject response) throws JSONException {
        Boolean status = response.getBoolean("status");
        if(status){
            JSONObject obj = response.getJSONObject("data");
            int ID= obj.getInt("item_id");
            String name= obj.getString("name");
            String desc1= obj.getString("desc_1");
            String desc2= obj.getString("desc_2");
            ItemQR item = new ItemQR();
            item.setId(ID);
            item.setName(name);
            item.setDescription1(desc1);
            item.setDescription2(desc2);
            realm.beginTransaction();
            realm.copyToRealm(item);
            realm.commitTransaction();
            Intent print = new Intent(getApplicationContext(),Print.class);
            print.putExtra("item_id",ID);
            startActivity(print);
        }else{
            Toast.makeText(this, response.getString("message"), Toast.LENGTH_LONG).show();
        }
    }
}
