package com.dasaline.beacukaiqrgenerator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by alanm on 8/1/2016.
 */
public class AppActivity extends AppCompatActivity
{
    public Realm realm;
    public Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(getApplicationContext()).build();
        Realm.setDefaultConfiguration(realmConfig);
        realm = Realm.getDefaultInstance();
        session=new Session(getApplicationContext());
    }
}
