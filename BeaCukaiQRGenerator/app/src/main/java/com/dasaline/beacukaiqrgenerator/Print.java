package com.dasaline.beacukaiqrgenerator;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.dasaline.beacukaiqrgenerator.Model.ItemQR;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONException;
import org.json.JSONObject;

public class Print extends AppActivity {
    private JSONObject obj;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        int ID = getIntent().getExtras().getInt("item_id");
        ItemQR qr = realm.where(ItemQR.class).equalTo("id", ID).findFirst();
        obj = new JSONObject();
        try {
            obj.put("name",qr.getName());
            obj.put("id",ID);
            obj.put("description",qr.getDescription1());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        TextView textCode = (TextView) findViewById(R.id.textViewItemID);
        textCode.setText("Item ID  : "+ID);
    }
    @Override
    protected void onStart() {
        super.onStart();
        String json=obj.toString();
        Bitmap myBitmap = QRCode.from(json).bitmap();
        if(myBitmap!=null) {
            ImageView qrImage = (ImageView) findViewById(R.id.imageViewQR);
            qrImage.setImageBitmap(myBitmap);
        }
    }
}
