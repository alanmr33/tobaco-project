package com.dasaline.beacukaiqrgenerator;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;


import cz.msebera.android.httpclient.Header;

public class Login extends AppCompatActivity {
    public AsyncHttpClient client;
    public Session session;
    public ProgressDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new Session(getApplicationContext());
        Button btnLogin = (Button) findViewById(R.id.buttonSignIn);
        final EditText textEmail= (EditText) findViewById(R.id.editTextEmail);
        final EditText textPassword= (EditText) findViewById(R.id.editTextPassword);
        client = new AsyncHttpClient();
        loading = new ProgressDialog(this);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=textEmail.getText().toString();
                String password=textPassword.getText().toString();
                if (email.equals("") || password.equals("")) {
                    Toast.makeText(getApplicationContext(),"Fill Username & Password",Toast.LENGTH_LONG).show();
                } else {
                    loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    loading.setMessage("Connecting to Server");
                    loading.setCanceledOnTouchOutside(false);
                    loading.setIndeterminate(true);
                    loading.show();
                    RequestParams params = new RequestParams();
                    params.put("username",email);
                    params.put("password",password);
                    client.post(Constants.APP_LOGIN_URL,params, new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                statusResponse(response);
                                loading.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }
    protected void statusResponse(JSONObject response) throws JSONException {
        Boolean status = response.getBoolean("status");
        if(status){
            session.setLogin(true);
            Intent intent = new Intent(getApplicationContext(), Main.class);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, response.getString("message"), Toast.LENGTH_LONG).show();
        }
    }

}
