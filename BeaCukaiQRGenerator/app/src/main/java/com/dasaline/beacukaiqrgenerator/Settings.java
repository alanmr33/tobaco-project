package com.dasaline.beacukaiqrgenerator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        EditText username = (EditText) findViewById(R.id.editTextEditProfileName);
        EditText email = (EditText) findViewById(R.id.editTextEditProfileEmail);
        username.setText("admin");
        email.setText("alanmr.0110@gmail.com");
    }
}
