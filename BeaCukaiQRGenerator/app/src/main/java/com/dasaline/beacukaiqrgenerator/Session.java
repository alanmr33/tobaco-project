package com.dasaline.beacukaiqrgenerator;

/**
 * Created by alan on 5/4/16.
 */
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;


public class Session {
    private static String TAG = Session.class.getSimpleName();
    SharedPreferences pref;
    Editor editor;
    Context _context;
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_LOCALID = "item_id";
    public Session(Context context) {
        this._context = context;
        pref = PreferenceManager.getDefaultSharedPreferences(_context);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
    }
    public void setID(int id) {
        editor.putInt(KEY_LOCALID, id);
        editor.commit();
    }
    public int ItemID(){
        return pref.getInt(KEY_LOCALID, 0);
    }
    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

}